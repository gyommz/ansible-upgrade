# Ansible playbook - Upgrade to DEV

## Command example to run (TLDR)

```shell
ansible-playbook -i inventory --limit luna_dev -u root -e VERSION_NAME=luna -e VERSION=2023.09 -e DOCKER_PASSWORD=my_token upgrade_dev.yml
```

Don't forget to replace :
* `luna_dev` with the lab (`luna_dev`, `kuma_dev`, `jabbah_dev` or `izar_dev`)
* `luna` with the lts name
* `2023.09` with the lts code
* `my_token` with the docker password for the xivoxc account

## Preconditions

* Install [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* Configure your [ssh config file with lab environment](https://gitlab.com/avencall/randd-doc/-/wikis/infrastructure/stun-turn-dev#mono-server-edge-lab-luna-)

## Variables

* VERSION : Major version numeral e.g. 2023.09
* VERSION_NAME : Major version name e.g. luna
* DOCKER_PASSWORD: The `xivoxc` docker password

## How to add a personal environment

* Add a new group like `ese_terraform_tf` with your machines IPs :
```yaml
[ese_terraform_tf]
10.181.31.2
10.181.31.3
10.181.31.4
10.181.31.5
10.181.31.6
```

* Add each machine in the right group (xivo, xivocc, mds, edge, meetingrooms):
```yaml
[xivo] 
10.181.31.2 # my xivo ip
luna_xivo
kuma_xivo
jabbah_xivo
izar_xivo
```
* You can now use your group in the command line (e.g. `--limit ese_terraform_tf`) 

## Tasks description

### all

* Switch XIVOCC_DIST to latestdev in custom.env

### XiVO && XiVO CC && Mds1

* change file `/etc/apt/sources.list.d/xivo-dist.list` with `xivo-{VERSION_name}-dev`

### XiVO

* xivo-upgrade

### XiVO CC

* xivocc-dcomp stop
* apt update
* apt dist-upgrade -y
* xivocc-dcomp pull
* xivocc-dcomp up -d

### Mds1

* mds-upgrade

### Edge && Meetingroom

* Switch XIVOCC_TAG to {VERSION} in .env

### Edge

* edge-dcomp stop
* apt update
* apt dist-upgrade -y
* edge-dcomp pull
* edge-dcomp up -d

### Meetingroom

* docker login -u xivoxc -p {DOCKER_PASSWORD}
* meetingroom-dcomp stop
* apt update
* apt dist-upgrade -y
* meetingroom-dcomp pull
* meetingroom-dcomp up -d
* docker logout

## Limitations

* Edge and Meetingrooms upgrade are not correct because yml files are not updated
* Execution could be speed up
* Variable are written rough, could be refined
* Inventory file can be filled with common labs